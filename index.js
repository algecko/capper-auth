const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')
const cors = require('cors')
const session = require('express-session')
const FileStore = require('session-file-store')(session)

const db = require('./db')
const config = require('./config.json')
const userMgmt = require('./userMgmt')
const tokenService = require('./tokenService')
const {basicAuth} = require('./auth')

const app = express()

const expirationHeader = 'Expires'

const sessionTimeout = config.sessionTimeout

const sess = {
	store: new FileStore({
		ttl: sessionTimeout,
		path: path.join(__dirname, './sessions')
	}),
	secret: 'awe$om€s3cr3T',
	resave: false,
	saveUninitialized: true,
	rolling: true,
	maxAge: sessionTimeout,
	cookie: {
		expires: sessionTimeout
	}
}

app.use(session(sess))

app.use(cors({
		origin: ['http://localhost:3000'], credentials: true
	}
))
app.use(bodyParser.json())

app.use((req, res, next) => {
	res.set(expirationHeader, '' + sessionTimeout)
	next()
})

app.post('/register', (req, res, next) => {
	const {username, password} = req.body
	userMgmt.register({username, password})
	.then(() => tokenService.getToken(username))
	.then(token => {
		req.session.username = username
		res.json(token)
	})
	.catch(next)
})

app.get('/login', basicAuth, (req, res, next) => {
	const sess = req.session
	sess.username = req.auth.user
	tokenService.getToken(req.auth.user)
	.then(token => res.json(token))
	.catch(next)
})

app.get('/token', (req, res, next) => {
	const sess = req.session
	if (!sess || !sess.username)
		return next({statusCode: 401})

	tokenService.getToken(sess.username)
	.then(token => res.json(token))
	.catch(next)
})

// todo remove once all services use new method
app.post('/token/verify', (req, res) => {
	tokenService.validateToken(req.body.token)
	.then(verifyResult => {
		if (!verifyResult.valid)
			return res.json({valid: false})
		// cheap hack for backward compatibility
		res.json({valid: true, token: {body: verifyResult.token}})
	})
})

app.put('/change-password', basicAuth, (req, res, next) => {
	if (!req.body.newPassword)
		return next({statusCode: 400, extMessage: 'newPassword needs to be provided'})
	userMgmt.changePassword({username: req.auth.user, newPassword: req.body.newPassword})
	.then(success => {
		if (!success)
			return next({statusCode: 500})

		return tokenService.getToken(req.auth.user)
	})
	.then(token => res.json(token))
	.catch(next)
})

app.get('/logout', (req, res, next) => {
	req.session.destroy((err) => {
		if (err)
			return next(err)

		res.status(204).clearCookie('connect.sid', {
			path: '/'
		}).end()
	})
})

app.get('/publicKey', (req, res) => {
	res.end(tokenService.getPublicKey())
})

app.use((req, res) => {
	res.status(404).end()
})

app.use((err, req, res, next) => {
	res.removeHeader(expirationHeader)

	const {extMessage, message, statusCode = 500} = err

	const logMessage = message || extMessage
	if (logMessage)
		console.error(logMessage)

	const myResponse = {}
	if (extMessage)
		myResponse.message = extMessage

	res.status(statusCode).json(myResponse)
})

const port = process.env.PORT || config.port
const dbUrl = process.env.DB_URL || config.dbUrl

db.connect(dbUrl)
.then(() => {
	app.listen(port, () => {
		console.log(`Started at port ${port}`)
	})
})
.catch(err => {
	console.error(err)
	process.exit(1)
})