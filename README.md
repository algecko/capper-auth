# About
Serves as the authentication tool passing JWTokens to be sent to any of the services.

## Needed to run
- config.json
    - With a path to a mongodb instance storing users (doesn't have to be a dedicated instance)
- keys
    - A folder in the root directory called *keys* containing a public key pair
        - private.key
        - public.pen
        - see https://www.npmjs.com/package/jsonwebtoken for details
        