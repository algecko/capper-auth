const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')
const config = require('./config')

const {promisify} = require('util')

const sign = (claims, expiry) => promisify(jwt.sign)(claims, privateKey, {algorithm: 'RS256', expiresIn: expiry})

const privateKey = fs.readFileSync(path.join(__dirname, './keys/private.key'))
const publicKey = fs.readFileSync(path.join(__dirname, './keys/public.pem'))

async function getToken (username) {
	const tokenExpiry = config.tokenTimout
	return sign({
		iss: 'capper-auth',
		sub: username,
		scope: 'creator'
	}, tokenExpiry)
	.then(signed => ({
		token: signed,
		expiresIn: tokenExpiry
	}))
}

const validateToken = (token) => new Promise((resolve) => {
	jwt.verify(token, publicKey, function (err, verifiedJwt) {
		if (err) {
			console.log(err)
			resolve({valid: false, error: err})
		} else {
			resolve({valid: true, token: verifiedJwt})
		}
	})
})

const getPublicKey = () => publicKey

module.exports = {getToken, getPublicKey, validateToken}