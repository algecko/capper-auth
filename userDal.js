const db = require('./db')

const users = () => db.get().collection('users')

const getUser = (username) => users().findOne({username: new RegExp(`^${username}$`, 'i')})

module.exports.getUser = getUser
module.exports.createUser = (user) => users().insert(user)
module.exports.exists = (username) => getUser(username).then(a => !!a)
module.exports.changePassword = (username, newPassword) => users().updateOne({username: username}, {$set: {password: newPassword}})