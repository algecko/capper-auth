const userMgmt = require('./userMgmt')

const AUTH_MODES = {
	basic: 'basic',
	none: 'none'
}

function basicAuth (req, res, next) {
	const authDetails = getAuthDetails(req)
	if (authDetails.mode !== AUTH_MODES.basic)
		return next({statusCode: 401})

	userMgmt.authenticate({username: authDetails.details.username, password: authDetails.details.password})
	.then(authResult => {
		if (!authResult.authenticated)
			return next({statusCode: 401})
		req.auth = {user: authResult.user.username}
		next()
	})
	.catch(next)
}

function getAuthDetails (req) {
	const authHeader = req.get('Authorization')
	if (authHeader) {

		const parts = authHeader.split(' ')

		if (parts[0].match(/^basic$/i))
			return {
				mode: AUTH_MODES.basic,
				details: getBasicAuthDetails(parts[1])
			}
	}

	return {
		mode: AUTH_MODES.none
	}
}

function getBasicAuthDetails (headerDetails) {
	const parts = (new Buffer(headerDetails, 'base64')).toString().split(':')
	return {
		username: parts[0],
		password: parts[1]
	}
}

module.exports = {basicAuth}