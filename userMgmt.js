const userDal = require('./userDal')
const config = require('./config')

const validateUsername = (username) => {
	const regex = /^([a-zA-Z]+)([a-zA-Z0-9\-]+)([0-9]*)$/g
	if (!username.match(regex))
		throw {
			statusCode: 400,
			extMessage: `Username '${username}' is invalid. Usernames must start with at lest 4 characters. Username can't start with numbers. Whitespaces and special characters are not allowed.`
		}
	return username
}

module.exports.register = async (options) => {
	const {username, password} = options
	if (!username || !password)
		throw {statusCode: 400, extMessage: 'username and password need to be provided'}

	if (await userDal.exists(username))
		throw {
			statusCode: 400,
			extMessage: `User '${username}' already exists`
		}

	if (config.reservedUsers.find(rn => rn.match(new RegExp(username, 'i'))))
		throw {
			statusCode: 400,
			message: `username '${username}' is reserved`,
			extMessage: `The username '${username}' is reserved because there are legacy posts posted with that name. Please contact an admin to get access to this username!`
		}

	return userDal.createUser({username: validateUsername(username), password})
}

module.exports.authenticate = async (options) => {
	const {username, password} = options
	if (!username || !password)
		throw {statusCode: 400, message: 'username and password need to be provided'}
	const user = await userDal.getUser(options.username)
	return {authenticated: user && user.password === password, user}
}

module.exports.changePassword = async (options) => {
	const {username, newPassword} = options
	if (!username || !newPassword)
		throw {statusCode: 400, message: 'username and newPassword need to be provided'}
	return userDal.changePassword(username, newPassword)
	.then(() => true)
	.catch(() => false)
}